cabal-version:      2.2
name:               boilerplate
version:            0.0.4
synopsis:           Generate Haskell boilerplate.
license:            GPL-3.0-or-later
license-file:       LICENSE
author:             Tseen She
maintainer:         Tseen She
copyright:          2020 Tseen She
tested-with:        GHC ^>=8.8.3 || ^>=8.10.7 || ^>=9.0.2 || ^>=9.2.7 || ^>=9.4.5 || ^>=9.6.1
category:           Building
description:
  Generates boilerplate from templates and markers in Haskell source code.

extra-source-files: README.md

source-repository head
  type:     git
  location: https://gitlab.com/tseenshe/boilerplate.git

-- https://www.haskell.org/cabal/users-guide/cabal-projectindex.html

flag ghcflags
  description: Generate .ghc.flags files during compilation
  manual:      True
  default:     False

common deps
  build-depends:
    , base          >=4.11   && <5
    , containers
    , directory
    , filepath
    , ghc
    , ghc-boot
    , hsinspect     ^>=0.0.17 || ^>=0.1.0
    , text
    , transformers

  ghc-options:
    -Wall -Werror=missing-home-modules -Werror=orphans
    -Wno-name-shadowing

  default-language: Haskell2010

  if flag(ghcflags)
    build-tool-depends: hsinspect:hsinspect -any
    build-depends:      ghcflags
    ghc-options:        -fplugin GhcFlags.Plugin

executable boilerplate
  import:         deps
  hs-source-dirs: exe
  main-is:        Main.hs
  ghc-options:    -rtsopts -threaded -with-rtsopts=-N8
  build-depends:
    , boilerplate
    , parsec       >3.1 && <3.2

library
  import:          deps
  hs-source-dirs:  library
  build-depends:
    , ghc-paths
    , parsers    >0.12 && <0.13
    , vector     >0.12 && <0.14

  -- cabal-fmt: expand library
  exposed-modules:
    Boilerplate.ConfigParser
    Boilerplate.Doc
    Boilerplate.GhcParser
    Boilerplate.Interpreter
    Boilerplate.RuleFinder
    Boilerplate.RuleParser
    Boilerplate.Types

test-suite test
  import:             deps
  hs-source-dirs:     test
  type:               exitcode-stdio-1.0
  main-is:            Driver.hs

  -- cabal-fmt: expand test -Driver
  other-modules:
    Boilerplate.ConfigParserTests
    Boilerplate.DocTests
    Boilerplate.InterpreterTests
    Boilerplate.RuleParserTests

  build-depends:
    , boilerplate
    , extra        ^>=1.7.8
    , HUnit        ^>=1.6.0
    , parsec
    , tasty        ^>=1.3.1
    , tasty-hspec  ^>=1.1.5

  build-tool-depends: tasty-discover:tasty-discover ^>=4.2.1
  ghc-options:        -threaded
