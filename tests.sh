#!/bin/bash

set -e -o pipefail

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "$SCRIPT_DIR"

# use cabal v2-configure to change ghc version

cabal build
cabal run test

BOILERPLATE=$(cabal exec -v0 which -- boilerplate)
# test for exceptions...
$BOILERPLATE --help
$BOILERPLATE --version
GHC_VERSION=$($BOILERPLATE --ghc-version)

$BOILERPLATE test-integration/Medley.hs > test-integration/Medley.output.hs
# to allow cabal script compilation / checking
chmod 755 test-integration/Medley.output.hs
test-integration/Medley.output.hs
$BOILERPLATE test-integration/Medley.output.hs > test-integration/Medley.idempotent

if ! diff -q test-integration/Medley.output.hs test-integration/Medley.idempotent ; then
    echo "FAILED IDEMPOTENCY CHECK"
    exit 1
fi

if ! git diff --quiet -- test-integration ; then
    echo "FAILED"
    git --no-pager diff -- test-integration
    exit 1
fi

echo "SUCCESS"
