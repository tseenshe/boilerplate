{-# LANGUAGE CPP #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}

module Boilerplate.GhcParser (parseHaskell) where

import Control.Monad.Trans.Except (runExceptT)
import Data.Either (fromRight)
import GHC.Paths (libdir)
import HsInspect.Runner
import HsInspect.Types (Comment(..), Type(..), types)

parseHaskell :: FilePath -> IO ([Type], [Comment])
parseHaskell file = do
  let fallback = ["-B" <> libdir]
  flags <- fromRight fallback <$> (runExceptT . ghcflags_flags $ Just file)
  runGhcAndJamMasterShe flags False $ types file
