#! /usr/bin/env cabal
{- cabal:
build-depends: base, text, unordered-containers, ffunctor, universum, aeson, QuickCheck
-}
-- with-compiler would be good...
-- https://github.com/haskell/cabal/issues/5698

{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE OverloadedStrings #-}
module Main where

import Control.Applicative ((<|>))
import Data.Aeson
import Data.Aeson.Encoding (emptyObject_, pair)
import Data.FFunctor
import Data.Foldable (toList)
import qualified Data.HashMap.Strict as H
import Data.Text (Text)
import qualified Data.Text as T
import Test.QuickCheck
import Universum ((...))

main :: IO ()
main = pure ()

-- testing fresh product expansion
data Coord1 = Coordy1 Double Double
{- BOILERPLATE Coord1 Show, Eq, Ord, FromJSON, ToJSON field=[xx,yy] -}
{- BOILERPLATE START -}
instance Show Coord1 where
  show (Coordy1 p_1_1 p_1_2) = concat ["Coordy1", show p_1_1, " ", show p_1_2]
instance Eq Coord1 where
  (Coordy1 p_1_1 p_1_2) == (Coordy1 p_2_1 p_2_2) = (p_1_1 == p_2_1) && (p_1_2 == p_2_2)
instance Ord Coord1 where
  (Coordy1 p_1_1 p_1_2) <= (Coordy1 p_2_1 p_2_2) = (p_1_1 <= p_2_1) && (p_1_2 <= p_2_2)
instance FromJSON Coord1 where
  parseJSON = withObject "Coord1" $ \v ->
    Coordy1 <$> v .: "xx" <*> v .: "yy"
instance ToJSON Coord1 where
  toJSON (Coordy1 p_1_1 p_1_2) = object ["xx" .= p_1_1, "yy" .= p_1_2]
  toEncoding (Coordy1 p_1_1 p_1_2) = pairs ("xx" .= p_1_1 <> "yy" .= p_1_2)
{- BOILERPLATE END -}
{- BOILERPLATE Coord1 Gen gen=["choose (0, 1)", "choose (1, 2)"] -}
{- BOILERPLATE START -}
genCoord1 :: Gen Coord1
genCoord1 = Coordy1 <$> choose (0, 1) <*> choose (1, 2)
{- BOILERPLATE END -}

-- testing replacement of existing rules (and products)
data Coord2 = Coordy2 { a :: Double, b :: Double}
-- BOILERPLATE Coord2 Show, Eq, Ord, FromJSON, ToJSON field={a:x, b:y}
{- BOILERPLATE START -}
instance Show Coord2 where
  show (Coordy2 p_1_1 p_1_2) = concat ["Coordy2", show p_1_1, " ", show p_1_2]
instance Eq Coord2 where
  (Coordy2 p_1_1 p_1_2) == (Coordy2 p_2_1 p_2_2) = (p_1_1 == p_2_1) && (p_1_2 == p_2_2)
instance Ord Coord2 where
  (Coordy2 p_1_1 p_1_2) <= (Coordy2 p_2_1 p_2_2) = (p_1_1 <= p_2_1) && (p_1_2 <= p_2_2)
instance FromJSON Coord2 where
  parseJSON = withObject "Coord2" $ \v ->
    Coordy2 <$> v .: "x" <*> v .: "y"
instance ToJSON Coord2 where
  toJSON (Coordy2 p_1_1 p_1_2) = object ["x" .= p_1_1, "y" .= p_1_2]
  toEncoding (Coordy2 p_1_1 p_1_2) = pairs ("x" .= p_1_1 <> "y" .= p_1_2)
{- BOILERPLATE END -}

-- testing expansion for sum
data Union = Wales String | England Int Int | Scotland
-- BOILERPLATE Union Show, Eq, Ord, FromJSON, ToJSON field={Wales:[name], England:[north,south]}
{- BOILERPLATE START -}
instance Show Union where
  show (Wales p_1_1) = concat ["Wales", show p_1_1]
  show (England p_1_1 p_1_2) = concat ["England", show p_1_1, " ", show p_1_2]
  show Scotland = "Scotland"
instance Eq Union where
  (Wales p_1_1) == (Wales p_2_1) = (p_1_1 == p_2_1)
  (England p_1_1 p_1_2) == (England p_2_1 p_2_2) = (p_1_1 == p_2_1) && (p_1_2 == p_2_2)
  Scotland == Scotland = True
instance Ord Union where
  (Wales p_1_1) <= (Wales p_2_1) = (p_1_1 <= p_2_1)
  (England p_1_1 p_1_2) <= (England p_2_1 p_2_2) = (p_1_1 <= p_2_1) && (p_1_2 <= p_2_2)
  Scotland <= Scotland = True
instance FromJSON Union where
  parseJSON = withObject "Union" $ \v ->
    let withField key parse = (maybe (fail "") pure $ H.lookup key v) >>= (withObject (T.unpack key) parse)
     in (withField "Wales" $ \v' -> Wales <$> v' .: "name")
    <|> (withField "England" $ \v' -> England <$> v' .: "north" <*> v' .: "south")
    <|> (withField "Scotland" $ \_ -> pure Scotland)
    <|> (fail "no valid type constructor tags")
instance ToJSON Union where
  toJSON (Wales p_1_1) = object ["Wales" .= object ["name" .= p_1_1]]
  toJSON (England p_1_1 p_1_2) = object ["England" .= object ["north" .= p_1_1, "south" .= p_1_2]]
  toJSON Scotland = object ["Scotland" .= object []]
  toEncoding (Wales p_1_1) = pairs . pair "Wales" $ pairs ("name" .= p_1_1 :: Series)
  toEncoding (England p_1_1 p_1_2) = pairs . pair "England" $ pairs ("north" .= p_1_1 <> "south" .= p_1_2 :: Series)
  toEncoding Scotland = pairs . pair "Scotland" $ emptyObject_
{- BOILERPLATE END -}
{- BOILERPLATE Union Gen -}
{- BOILERPLATE START -}
genUnion :: Gen Union
genUnion = oneof [
  Wales <$> genString,
  England <$> genInt <*> genInt,
  pure Scotland]
{- BOILERPLATE END -}

genInt :: Gen Int
genInt = choose (1, 100)

genString :: Gen String
genString = listOf arbitraryASCIIChar

data Stranger a = Stranger a Double
{- BOILERPLATE Stranger Show, Eq, Ord, Functor, Foldable, Traversable -}
{- BOILERPLATE START -}
instance (Show a) => Show (Stranger a) where
  show (Stranger p_1_1 p_1_2) = concat ["Stranger", show p_1_1, " ", show p_1_2]
instance (Eq a) => Eq (Stranger a) where
  (Stranger p_1_1 p_1_2) == (Stranger p_2_1 p_2_2) = (p_1_1 == p_2_1) && (p_1_2 == p_2_2)
instance (Ord a) => Ord (Stranger a) where
  (Stranger p_1_1 p_1_2) <= (Stranger p_2_1 p_2_2) = (p_1_1 <= p_2_1) && (p_1_2 <= p_2_2)
instance Functor Stranger where
  fmap f (Stranger p_1_1 p_1_2) = Stranger (f p_1_1) p_1_2
instance Foldable Stranger where
  foldr f z (Stranger p_1_1 p_1_2) = foldr f z $ concat [[p_1_1], (const [] p_1_2)]
instance Traversable Stranger where
  traverse f (Stranger p_1_1 p_1_2) = Stranger <$> (f p_1_1) <*> (pure p_1_2)
{- BOILERPLATE END -}

data Things a b = T a b Double

data Logger (m :: * -> *) = Logger { log :: Text -> m () }
{- BOILERPLATE Logger FFunctor -}
{- BOILERPLATE START -}
instance FFunctor Logger where
  ffmap nt (Logger p_1_1) = Logger (nt ... p_1_1)
{- BOILERPLATE END -}

data Action a = Admin [a] Text | User a Text | Guest
{- BOILERPLATE Action Show, Eq, Ord, Functor, Foldable, Traversable -}
{- BOILERPLATE START -}
instance (Show a) => Show (Action a) where
  show (Admin p_1_1 p_1_2) = concat ["Admin", show p_1_1, " ", show p_1_2]
  show (User p_1_1 p_1_2) = concat ["User", show p_1_1, " ", show p_1_2]
  show Guest = "Guest"
instance (Eq a) => Eq (Action a) where
  (Admin p_1_1 p_1_2) == (Admin p_2_1 p_2_2) = (p_1_1 == p_2_1) && (p_1_2 == p_2_2)
  (User p_1_1 p_1_2) == (User p_2_1 p_2_2) = (p_1_1 == p_2_1) && (p_1_2 == p_2_2)
  Guest == Guest = True
instance (Ord a) => Ord (Action a) where
  (Admin p_1_1 p_1_2) <= (Admin p_2_1 p_2_2) = (p_1_1 <= p_2_1) && (p_1_2 <= p_2_2)
  (User p_1_1 p_1_2) <= (User p_2_1 p_2_2) = (p_1_1 <= p_2_1) && (p_1_2 <= p_2_2)
  Guest <= Guest = True
instance Functor Action where
  fmap f (Admin p_1_1 p_1_2) = Admin (fmap f p_1_1) p_1_2
  fmap f (User p_1_1 p_1_2) = User (f p_1_1) p_1_2
  fmap f Guest = Guest
instance Foldable Action where
  foldr f z (Admin p_1_1 p_1_2) = foldr f z $ concat [(toList p_1_1), (const [] p_1_2)]
  foldr f z (User p_1_1 p_1_2) = foldr f z $ concat [[p_1_1], (const [] p_1_2)]
  foldr f z Guest = foldr f z $ concat []
instance Traversable Action where
  traverse f (Admin p_1_1 p_1_2) = Admin <$> (traverse f p_1_1) <*> (pure p_1_2)
  traverse f (User p_1_1 p_1_2) = User <$> (f p_1_1) <*> (pure p_1_2)
  traverse f Guest = pure Guest
{- BOILERPLATE END -}

data InfixProd = Text :| Text
{- BOILERPLATE InfixProd Show, Eq, Ord -}
{- BOILERPLATE START -}
instance Show InfixProd where
  show ((:|) p_1_1 p_1_2) = concat ["(:|)", show p_1_1, " ", show p_1_2]
instance Eq InfixProd where
  ((:|) p_1_1 p_1_2) == ((:|) p_2_1 p_2_2) = (p_1_1 == p_2_1) && (p_1_2 == p_2_2)
instance Ord InfixProd where
  ((:|) p_1_1 p_1_2) <= ((:|) p_2_1 p_2_2) = (p_1_1 <= p_2_1) && (p_1_2 <= p_2_2)
{- BOILERPLATE END -}

data InfixSum = Text :|| Text
              | Text :||| Text
{- BOILERPLATE InfixSum Show, Eq, Ord -}
{- BOILERPLATE START -}
instance Show InfixSum where
  show ((:||) p_1_1 p_1_2) = concat ["(:||)", show p_1_1, " ", show p_1_2]
  show ((:|||) p_1_1 p_1_2) = concat ["(:|||)", show p_1_1, " ", show p_1_2]
instance Eq InfixSum where
  ((:||) p_1_1 p_1_2) == ((:||) p_2_1 p_2_2) = (p_1_1 == p_2_1) && (p_1_2 == p_2_2)
  ((:|||) p_1_1 p_1_2) == ((:|||) p_2_1 p_2_2) = (p_1_1 == p_2_1) && (p_1_2 == p_2_2)
instance Ord InfixSum where
  ((:||) p_1_1 p_1_2) <= ((:||) p_2_1 p_2_2) = (p_1_1 <= p_2_1) && (p_1_2 <= p_2_2)
  ((:|||) p_1_1 p_1_2) <= ((:|||) p_2_1 p_2_2) = (p_1_1 <= p_2_1) && (p_1_2 <= p_2_2)
{- BOILERPLATE END -}
