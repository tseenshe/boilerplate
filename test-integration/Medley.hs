#! /usr/bin/env cabal
{- cabal:
build-depends: base, text, unordered-containers, ffunctor, universum, aeson, QuickCheck
-}
-- with-compiler would be good...
-- https://github.com/haskell/cabal/issues/5698

{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE OverloadedStrings #-}
module Main where

import Control.Applicative ((<|>))
import Data.Aeson
import Data.Aeson.Encoding (emptyObject_, pair)
import Data.FFunctor
import Data.Foldable (toList)
import qualified Data.HashMap.Strict as H
import Data.Text (Text)
import qualified Data.Text as T
import Test.QuickCheck
import Universum ((...))

main :: IO ()
main = pure ()

-- testing fresh product expansion
data Coord1 = Coordy1 Double Double
{- BOILERPLATE Coord1 Show, Eq, Ord, FromJSON, ToJSON field=[xx,yy] -}
{- BOILERPLATE Coord1 Gen gen=["choose (0, 1)", "choose (1, 2)"] -}

-- testing replacement of existing rules (and products)
data Coord2 = Coordy2 { a :: Double, b :: Double}
-- BOILERPLATE Coord2 Show, Eq, Ord, FromJSON, ToJSON field={a:x, b:y}

-- testing expansion for sum
data Union = Wales String | England Int Int | Scotland
-- BOILERPLATE Union Show, Eq, Ord, FromJSON, ToJSON field={Wales:[name], England:[north,south]}
{- BOILERPLATE Union Gen -}

genInt :: Gen Int
genInt = choose (1, 100)

genString :: Gen String
genString = listOf arbitraryASCIIChar

data Stranger a = Stranger a Double
{- BOILERPLATE Stranger Show, Eq, Ord, Functor, Foldable, Traversable -}

data Things a b = T a b Double

data Logger (m :: * -> *) = Logger { log :: Text -> m () }
{- BOILERPLATE Logger FFunctor -}

data Action a = Admin [a] Text | User a Text | Guest
{- BOILERPLATE Action Show, Eq, Ord, Functor, Foldable, Traversable -}

data InfixProd = Text :| Text
{- BOILERPLATE InfixProd Show, Eq, Ord -}

data InfixSum = Text :|| Text
              | Text :||| Text
{- BOILERPLATE InfixSum Show, Eq, Ord -}
